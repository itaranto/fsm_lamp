#include "fsm.h"

#include <stdio.h>

#define DEFINE_ACTION_DUMMY(name)                         \
static void name() {                                      \
    printf("Transition: " #name " State: %d\n", current); \
}

#define DEFINE_ACTION_BASE(name, action)                  \
static void name() {                                      \
    printf("Transition: " #name " State: %d\n", current); \
    action();                                             \
}

typedef enum {
    STATE_OFF_UNPLUGGED,
    STATE_OFF_PLUGGED,
    STATE_MID_UNPLUGGED,
    STATE_MID_PLUGGED,
    STATE_WAITING_TO_FULL,
    STATE_FULL,
    STATE_INVALID,
    STATE_COUNT /* Always keep last */
} fsm_State;

typedef void (*fsm_Action)(void);

typedef struct {
    fsm_State  state;
    fsm_Action action;
} fsm_Transition;

static fsm_State current = STATE_OFF_UNPLUGGED;

// Actions

static void fsm_turn_off() {
    printf("ACTION: turn_off\n");
}

static void fsm_turn_middle() {
    printf("ACTION: turn_middle\n");
}

static void fsm_turn_full() {
    printf("ACTION: turn_full\n");
}

static void fsm_invalid() {
    printf("ACTION: invalid\n");
}

DEFINE_ACTION_DUMMY(off_at_off_unplugged)
DEFINE_ACTION_BASE(on_at_off_unplugged,     fsm_turn_middle)
DEFINE_ACTION_BASE(unplug_at_off_unplugged, fsm_invalid)
DEFINE_ACTION_DUMMY(plug_at_off_unplugged   /* Nothing */)
DEFINE_ACTION_DUMMY(off_at_off_plugged      /* Nothing */)
DEFINE_ACTION_BASE(on_at_off_plugged,       fsm_turn_middle)
DEFINE_ACTION_DUMMY(unplug_at_off_plugged   /* Nothing */)
DEFINE_ACTION_BASE(plug_at_off_plugged,     fsm_invalid)
DEFINE_ACTION_BASE(off_at_mid_unplugged,    fsm_turn_off)
DEFINE_ACTION_DUMMY(on_at_mid_unplugged     /* Nothing */)
DEFINE_ACTION_BASE(unplug_at_mid_unplugged, fsm_invalid)
DEFINE_ACTION_DUMMY(plug_at_mid_unplugged   /* Nothing */)
DEFINE_ACTION_BASE(off_at_mid_plugged,      fsm_turn_off)
DEFINE_ACTION_BASE(on_at_mid_plugged,       fsm_turn_full)
DEFINE_ACTION_DUMMY(unplug_at_mid_plugged   /* Nothing */)
DEFINE_ACTION_BASE(plug_at_mid_plugged,     fsm_invalid)
DEFINE_ACTION_BASE(off_at_wtf,              fsm_turn_middle)
DEFINE_ACTION_DUMMY(on_at_wtf               /* Nothing */)
DEFINE_ACTION_BASE(unplug_at_wtf,           fsm_invalid)
DEFINE_ACTION_BASE(plug_at_wtf,             fsm_turn_full)
DEFINE_ACTION_BASE(off_at_full,             fsm_turn_middle)
DEFINE_ACTION_DUMMY(on_at_full              /* Nothing */)
DEFINE_ACTION_BASE(unplug_at_full,          fsm_turn_middle)
DEFINE_ACTION_BASE(plug_at_full,            fsm_invalid)
DEFINE_ACTION_BASE(off_at_invalid,          fsm_invalid)
DEFINE_ACTION_BASE(on_at_invalid,           fsm_invalid)
DEFINE_ACTION_BASE(unplug_at_invalid,       fsm_invalid)
DEFINE_ACTION_BASE(plug_at_invalid,         fsm_invalid)

// Transitions

static const fsm_Transition transition_table[EVENT_COUNT][STATE_COUNT] =
{
    { /* EVENT_OFF */
        { STATE_OFF_UNPLUGGED,   off_at_off_unplugged    },
        { STATE_OFF_PLUGGED,     off_at_off_plugged      },
        { STATE_OFF_UNPLUGGED,   off_at_mid_unplugged    },
        { STATE_OFF_PLUGGED,     off_at_mid_plugged      },
        { STATE_MID_UNPLUGGED,   off_at_wtf              },
        { STATE_MID_PLUGGED,     off_at_full             },
        { STATE_INVALID,         off_at_invalid          }
    },
    { /* EVENT_ON */
        { STATE_MID_UNPLUGGED,   on_at_off_unplugged     },
        { STATE_MID_PLUGGED,     on_at_off_plugged       },
        { STATE_WAITING_TO_FULL, on_at_mid_unplugged     },
        { STATE_FULL,            on_at_mid_plugged       },
        { STATE_WAITING_TO_FULL, on_at_wtf               },
        { STATE_FULL,            on_at_full              },
        { STATE_INVALID,         on_at_invalid           }
    },
    { /* EVENT_PLUG */
        { STATE_OFF_PLUGGED,     plug_at_off_unplugged   },
        { STATE_INVALID,         plug_at_off_plugged     },
        { STATE_MID_PLUGGED,     plug_at_mid_unplugged   },
        { STATE_INVALID,         plug_at_mid_plugged     },
        { STATE_FULL,            plug_at_wtf             },
        { STATE_INVALID,         plug_at_full            },
        { STATE_INVALID,         plug_at_invalid         }
   },
   { /* EVENT_UNPLUG */
        { STATE_INVALID,         unplug_at_off_unplugged },
        { STATE_OFF_UNPLUGGED,   unplug_at_off_plugged   },
        { STATE_INVALID,         unplug_at_mid_unplugged },
        { STATE_MID_UNPLUGGED,   unplug_at_mid_plugged   },
        { STATE_INVALID,         unplug_at_wtf           },
        { STATE_WAITING_TO_FULL, unplug_at_full          },
        { STATE_INVALID,         unplug_at_invalid       }

    }
};

void fsm_update(const fsm_Event event) {
    transition_table[event][current].action();
    current = transition_table[event][current].state;
}

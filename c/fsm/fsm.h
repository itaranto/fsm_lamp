#ifndef FSM_H
#define FSM_H

typedef enum {
    EVENT_OFF,
    EVENT_ON,
    EVENT_PLUG,
    EVENT_UNPLUG,
    EVENT_COUNT /* Always keep last */
} fsm_Event;

void fsm_update(fsm_Event event);

#endif /* FSM_H */

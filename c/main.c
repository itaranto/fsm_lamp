#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fsm.h"

#define MAX_INPUT_SIZE 4

int main(void) {
    char line[MAX_INPUT_SIZE + 1] = { 0 };

    while (true) {
        const char* s = fgets(line, MAX_INPUT_SIZE, stdin);
        if (!s) {
            printf("Error or EOF when reading line\n");
            break;
        }

        /* atoi returns 0 when failure, so we need to handle this special case. */
        const fsm_Event event = atoi(line);
        if (event == 0 && strncmp(line, "0\n", 2) != 0) {
            printf("Cannot convert to integer, please try again\n");
        } else if (event < 0 || event >= EVENT_COUNT) {
            printf("Invalid event, please try again\n");
        } else {
            fsm_update(event);
        }
    }

    return 0;
}

#ifndef __FSM__
#define __FSM__

#include <memory>
#include <string>

namespace fsm {
    class State {
    public:
        virtual ~State() = default;

        // Events
        virtual std::unique_ptr<State> off() const = 0;
        virtual std::unique_ptr<State> on() const = 0;
        virtual std::unique_ptr<State> plug() const = 0;
        virtual std::unique_ptr<State> unplug() const = 0;

        virtual const std::string to_string() const = 0;

    protected:
        State() = default;

        // Actions
        void turn_off() const;
        void turn_middle() const;
        void turn_full() const;
        void invalid() const;
    };

    class Full : public State {
    public:
        // Events
        std::unique_ptr<State> off() const override;
        std::unique_ptr<State> on() const override;
        std::unique_ptr<State> plug() const override;
        std::unique_ptr<State> unplug() const override;

        const std::string to_string() const override;
     };

    class Invalid : public State {
    public:
        // Events
        std::unique_ptr<State> off() const override;
        std::unique_ptr<State> on() const override;
        std::unique_ptr<State> plug() const override;
        std::unique_ptr<State> unplug() const override;

        const std::string to_string() const override;
     };

    class MidPlugged : public State {
    public:
        // Events
        std::unique_ptr<State> off() const override;
        std::unique_ptr<State> on() const override;
        std::unique_ptr<State> plug() const override;
        std::unique_ptr<State> unplug() const override;

        const std::string to_string() const override;
    };

    class MidUnplugged : public State {
    public:
        // Events
        std::unique_ptr<State> off() const override;
        std::unique_ptr<State> on() const override;
        std::unique_ptr<State> plug() const override;
        std::unique_ptr<State> unplug() const override;

        const std::string to_string() const override;
    };

    class OffPlugged : public State {
    public:
        // Events
        std::unique_ptr<State> off() const override;
        std::unique_ptr<State> on() const override;
        std::unique_ptr<State> plug() const override;
        std::unique_ptr<State> unplug() const override;

        const std::string to_string() const override;
    };

    class OffUnplugged : public State {
    public:
        // Events
        std::unique_ptr<State> off() const override;
        std::unique_ptr<State> on() const override;
        std::unique_ptr<State> plug() const override;
        std::unique_ptr<State> unplug() const override;

        const std::string to_string() const override;
    };

    class WaitingToFull : public State {
    public:
        // Events
        std::unique_ptr<State> off() const override;
        std::unique_ptr<State> on() const override;
        std::unique_ptr<State> plug() const override;
        std::unique_ptr<State> unplug() const override;

        const std::string to_string() const override;
    };

    class Lamp {
    public:
        Lamp();

        Lamp(const Lamp&) = delete;
        Lamp& operator=(const Lamp&) = delete;
        ~Lamp() = default;

        // Events
        void off();
        void on();
        void plug();
        void unplug();

    private:
        void update(std::unique_ptr<State> state);

        std::unique_ptr<State> m_state;
    };
}

#endif //__FSM__

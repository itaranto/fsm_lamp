package fsm

import (
	"fmt"
)

// State

type State interface {
	// Events
	off() StateStringer
	on() StateStringer
	plug() StateStringer
	unplug() StateStringer

	// Actions
	turnOff()
	turnMiddle()
	turnFull()
	invalid()
}

type StateStringer interface {
	State
	String() string
}

// Full

type Full struct{}

func (f *Full) off() StateStringer {
	f.turnMiddle()
	return &MidPlugged{}
}

func (f *Full) on() StateStringer {
	return f
}

func (f *Full) plug() StateStringer {
	f.invalid()
	return &Invalid{}
}

func (f *Full) unplug() StateStringer {
	f.turnMiddle()
	return &WaitingToFull{}
}

func (f *Full) String() string {
	return "Full"
}

func (f *Full) turnOff() {
	fmt.Println("ACTION: turn_off")
}

func (f *Full) turnMiddle() {
	fmt.Println("ACTION: turn_middle")
}

func (f *Full) turnFull() {
	fmt.Println("ACTION: turn_full")
}

func (f *Full) invalid() {
	fmt.Println("ACTION: invalid")
}

// Invalid

type Invalid struct{}

func (i *Invalid) off() StateStringer {
	i.invalid()
	return i
}

func (i *Invalid) on() StateStringer {
	i.invalid()
	return i
}

func (i *Invalid) plug() StateStringer {
	i.invalid()
	return i
}

func (i *Invalid) unplug() StateStringer {
	i.invalid()
	return i
}

func (i *Invalid) String() string {
	return "Invalid"
}

func (i *Invalid) turnOff() {
	fmt.Println("ACTION: turn_off")
}

func (i *Invalid) turnMiddle() {
	fmt.Println("ACTION: turn_middle")
}

func (i *Invalid) turnFull() {
	fmt.Println("ACTION: turn_full")
}

func (i *Invalid) invalid() {
	fmt.Println("ACTION: invalid")
}

// MidPlugged

type MidPlugged struct{}

func (m *MidPlugged) off() StateStringer {
	m.turnOff()
	return &OffPlugged{}
}

func (m *MidPlugged) on() StateStringer {
	m.turnFull()
	return &Full{}
}

func (m *MidPlugged) plug() StateStringer {
	m.invalid()
	return &Invalid{}
}

func (m *MidPlugged) unplug() StateStringer {
	return &MidUnplugged{}
}

func (m *MidPlugged) String() string {
	return "Mid plugged"
}

func (m *MidPlugged) turnOff() {
	fmt.Println("ACTION: turn_off")
}

func (m *MidPlugged) turnMiddle() {
	fmt.Println("ACTION: turn_middle")
}

func (m *MidPlugged) turnFull() {
	fmt.Println("ACTION: turn_full")
}

func (m *MidPlugged) invalid() {
	fmt.Println("ACTION: invalid")
}

// MidUnplugged

type MidUnplugged struct{}

func (m *MidUnplugged) off() StateStringer {
	m.turnOff()
	return &OffUnplugged{}
}

func (m *MidUnplugged) on() StateStringer {
	return &WaitingToFull{}
}

func (m *MidUnplugged) plug() StateStringer {
	return &MidPlugged{}
}

func (m *MidUnplugged) unplug() StateStringer {
	m.invalid()
	return &Invalid{}
}

func (m *MidUnplugged) String() string {
	return "Mid unplugged"
}

func (m *MidUnplugged) turnOff() {
	fmt.Println("ACTION: turn_off")
}

func (m *MidUnplugged) turnMiddle() {
	fmt.Println("ACTION: turn_middle")
}

func (m *MidUnplugged) turnFull() {
	fmt.Println("ACTION: turn_full")
}

func (m *MidUnplugged) invalid() {
	fmt.Println("ACTION: invalid")
}

// OffPlugged

type OffPlugged struct{}

func (o *OffPlugged) off() StateStringer {
	return &MidPlugged{}
}

func (o *OffPlugged) on() StateStringer {
	o.turnMiddle()
	return o
}

func (o *OffPlugged) plug() StateStringer {
	o.invalid()
	return &Invalid{}
}

func (o *OffPlugged) unplug() StateStringer {
	return &OffUnplugged{}
}

func (o *OffPlugged) String() string {
	return "Off plugged"
}

func (o *OffPlugged) turnOff() {
	fmt.Println("ACTION: turn_off")
}

func (o *OffPlugged) turnMiddle() {
	fmt.Println("ACTION: turn_middle")
}

func (o *OffPlugged) turnFull() {
	fmt.Println("ACTION: turn_full")
}

func (o *OffPlugged) invalid() {
	fmt.Println("ACTION: invalid")
}

// OffUnplugged

type OffUnplugged struct{}

func (o *OffUnplugged) off() StateStringer {
	return o
}

func (o *OffUnplugged) on() StateStringer {
	o.turnMiddle()
	return &MidUnplugged{}
}

func (o *OffUnplugged) plug() StateStringer {
	return &OffPlugged{}
}

func (o *OffUnplugged) unplug() StateStringer {
	o.invalid()
	return &Invalid{}
}

func (o *OffUnplugged) String() string {
	return "Off unplugged"
}

func (o *OffUnplugged) turnOff() {
	fmt.Println("ACTION: turn_off")
}

func (o *OffUnplugged) turnMiddle() {
	fmt.Println("ACTION: turn_middle")
}

func (o *OffUnplugged) turnFull() {
	fmt.Println("ACTION: turn_full")
}

func (o *OffUnplugged) invalid() {
	fmt.Println("ACTION: invalid")
}

// WaitingToFull

type WaitingToFull struct{}

func (w *WaitingToFull) off() StateStringer {
	w.turnMiddle()
	return &MidUnplugged{}
}

func (w *WaitingToFull) on() StateStringer {
	return w
}

func (w *WaitingToFull) plug() StateStringer {
	w.turnFull()
	return &Full{}
}

func (w *WaitingToFull) unplug() StateStringer {
	return &Invalid{}
}

func (w *WaitingToFull) String() string {
	return "Waiting to full"
}

func (w *WaitingToFull) turnOff() {
	fmt.Println("ACTION: turn_off")
}

func (w *WaitingToFull) turnMiddle() {
	fmt.Println("ACTION: turn_middle")
}

func (w *WaitingToFull) turnFull() {
	fmt.Println("ACTION: turn_full")
}

func (w *WaitingToFull) invalid() {
	fmt.Println("ACTION: invalid")
}

// Lamp

type Lamp struct {
	state StateStringer
}

func NewLamp() *Lamp {
	return &Lamp{state: &OffUnplugged{}}
}

func (l *Lamp) Off() {
	l.update(l.state.off())
}

func (l *Lamp) On() {
	l.update(l.state.on())
}

func (l *Lamp) Plug() {
	l.update(l.state.plug())
}

func (l *Lamp) Unplug() {
	l.update(l.state.unplug())
}

func (l *Lamp) update(state StateStringer) {
	fmt.Printf("Transition from: %s to: %s\n", l.state, state)
	l.state = state
}

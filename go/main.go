package main

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"

	"gitlab.com/fsm-lamp/lamp/fsm"
)

type event int

const (
	off event = iota
	on
	plug
	unplug
)

func dispatchEvent(event event, lamp *fsm.Lamp) {
	switch event {
	case off:
		lamp.Off()
	case on:
		lamp.On()
	case plug:
		lamp.Plug()
	case unplug:
		lamp.Unplug()
	default:
		fmt.Println("Invalid event, please try again")
	}
}

func main() {
	lamp := fsm.NewLamp()

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		ev, err := strconv.Atoi(scanner.Text())
		if err != nil {
			if errors.Is(err, strconv.ErrSyntax) {
				fmt.Println("Cannot convert to integer, please try again")
			} else if !errors.Is(err, strconv.ErrRange) {
				fmt.Println("Integer out of range, please try again")
			} else {
				panic(err)
			}

			continue
		}

		dispatchEvent(event(ev), lamp)
	}

	if err := scanner.Err(); err != nil {
		log.Fatalln("Error scanning input: ", err)
	}
}

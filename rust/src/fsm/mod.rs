trait State {
    // Events
    fn off(self: Box<Self>) -> Box<dyn State>;
    fn on(self: Box<Self>) -> Box<dyn State>;
    fn plug(self: Box<Self>) -> Box<dyn State>;
    fn unplug(self: Box<Self>) -> Box<dyn State>;

    fn to_string(self: &Self) -> String;

    // Actions
    fn turn_off(self: &Self) {
        println!("ACTION: turn_off");
    }

    fn turn_middle(self: &Self) {
        println!("ACTION: turn_middle");
    }

    fn turn_full(self: &Self) {
        println!("ACTION: turn_full");
    }

    fn invalid(self: &Self) {
        println!("ACTION: invalid");
    }
}

struct Full {}

impl State for Full {
    fn off(self: Box<Self>) -> Box<dyn State> {
        self.turn_middle();
        Box::new(MidPlugged {})
    }

    fn on(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn plug(self: Box<Self>) -> Box<dyn State> {
        self.invalid();
        Box::new(Invalid {})
    }

    fn unplug(self: Box<Self>) -> Box<dyn State> {
        self.turn_middle();
        Box::new(WaitingToFull {})
    }

    fn to_string(self: &Self) -> String {
        "Full".to_string()
    }
}

struct Invalid {}

impl State for Invalid {
    fn off(self: Box<Self>) -> Box<dyn State> {
        self.invalid();
        self
    }

    fn on(self: Box<Self>) -> Box<dyn State> {
        self.invalid();
        self
    }

    fn plug(self: Box<Self>) -> Box<dyn State> {
        self.invalid();
        self
    }

    fn unplug(self: Box<Self>) -> Box<dyn State> {
        self.invalid();
        self
    }

    fn to_string(self: &Self) -> String {
        "Invalid".to_string()
    }
}

struct MidPlugged {}

impl State for MidPlugged {
    fn off(self: Box<Self>) -> Box<dyn State> {
        self.turn_off();
        Box::new(OffPlugged {})
    }

    fn on(self: Box<Self>) -> Box<dyn State> {
        self.turn_full();
        Box::new(Full {})
    }

    fn plug(self: Box<Self>) -> Box<dyn State> {
        self.invalid();
        Box::new(Invalid {})
    }

    fn unplug(self: Box<Self>) -> Box<dyn State> {
        Box::new(MidUnplugged {})
    }

    fn to_string(self: &Self) -> String {
        "Mid plugged".to_string()
    }
}

struct MidUnplugged {}

impl State for MidUnplugged {
    fn off(self: Box<Self>) -> Box<dyn State> {
        self.turn_off();
        Box::new(OffUnplugged {})
    }

    fn on(self: Box<Self>) -> Box<dyn State> {
        Box::new(WaitingToFull {})
    }

    fn plug(self: Box<Self>) -> Box<dyn State> {
        Box::new(MidPlugged {})
    }

    fn unplug(self: Box<Self>) -> Box<dyn State> {
        self.invalid();
        Box::new(Invalid {})
    }

    fn to_string(self: &Self) -> String {
        "Mid unplugged".to_string()
    }
}

struct OffPlugged {}

impl State for OffPlugged {
    fn off(self: Box<Self>) -> Box<dyn State> {
        Box::new(MidPlugged {})
    }

    fn on(self: Box<Self>) -> Box<dyn State> {
        self.turn_middle();
        self
    }

    fn plug(self: Box<Self>) -> Box<dyn State> {
        self.invalid();
        Box::new(Invalid {})
    }

    fn unplug(self: Box<Self>) -> Box<dyn State> {
        Box::new(OffUnplugged {})
    }

    fn to_string(self: &Self) -> String {
        "Off plugged".to_string()
    }
}

struct OffUnplugged {}

impl State for OffUnplugged {
    fn off(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn on(self: Box<Self>) -> Box<dyn State> {
        self.turn_middle();
        Box::new(MidUnplugged {})
    }

    fn plug(self: Box<Self>) -> Box<dyn State> {
        Box::new(MidUnplugged {})
    }

    fn unplug(self: Box<Self>) -> Box<dyn State> {
        self.invalid();
        Box::new(Invalid {})
    }

    fn to_string(self: &Self) -> String {
        "Off unplugged".to_string()
    }
}

struct WaitingToFull {}

impl State for WaitingToFull {
    fn off(self: Box<Self>) -> Box<dyn State> {
        self.turn_middle();
        Box::new(MidUnplugged {})
    }

    fn on(self: Box<Self>) -> Box<dyn State> {
        self
    }

    fn plug(self: Box<Self>) -> Box<dyn State> {
        self.turn_full();
        Box::new(Full {})
    }

    fn unplug(self: Box<Self>) -> Box<dyn State> {
        Box::new(Invalid {})
    }

    fn to_string(self: &Self) -> String {
        "Waiting to full".to_string()
    }
}

pub struct Lamp {
    state: Option<Box<dyn State>>,
}

impl Lamp {
    pub fn new() -> Lamp {
        Lamp {
            state: Some(Box::new(OffUnplugged {})),
        }
    }

    pub fn off(&mut self) {
        if let Some(s) = self.state.take() {
            println!("Transition from: {}", s.to_string());
            let state = s.off();
            println!("Transition to: {}", state.to_string());
            self.state = Some(state)
        }
    }

    pub fn on(&mut self) {
        if let Some(s) = self.state.take() {
            println!("Transition from: {}", s.to_string());
            let state = s.on();
            println!("Transition to: {}", state.to_string());
            self.state = Some(state)
        }
    }

    pub fn plug(&mut self) {
        if let Some(s) = self.state.take() {
            println!("Transition from: {}", s.to_string());
            let state = s.plug();
            println!("Transition to: {}", state.to_string());
            self.state = Some(state)
        }
    }

    pub fn unplug(&mut self) {
        if let Some(s) = self.state.take() {
            println!("Transition from: {}", s.to_string());
            let state = s.unplug();
            println!("Transition to: {}", state.to_string());
            self.state = Some(state)
        }
    }
}
